<?php

define("DB_HOST", "localhost");
define("DB_USER", "root"); // FIXME: It's best to use a different user.
// CREATE USER 'insulation'@'localhost' IDENTIFIED BY 'some_pass';
// GRANT ALL ON sprayfoam_insulation.* TO 'insulation'@'localhost';

// This prevents them from wrecking havoc on all your databases.

define("DB_PASS", "");
define("DB_NAME", "grades");