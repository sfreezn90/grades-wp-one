<?php
/**
 * Created by PhpStorm.
 * User: Kent
 * Date: 6/29/2017
 * Time: 4:10 PM
 */

define("HTTP_OK", 200);
define("HTTP_NO_CONTENT", 204);
define("HTTP_NOT_MODIFIED", 304);
define("HTTP_BAD_REQUEST", 400);
define("HTTP_NOT_FOUND", 404);
define("HTTP_METHOD_NOT_ALLOWED", 405);
define("HTTP_INTERNAL_ERROR", 500);

function set_status_code($code) {
    if(is_callable('http_response_code')) {
        http_response_code($code);
    } else {
        // Arvixe is running on PHP 5.3, so it does not support http_response_code.
        header('X-Set-PHP-Status', true, $code);
    }
}