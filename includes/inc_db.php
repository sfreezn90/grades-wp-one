<?php

if(!defined("ROOT_DIR")) {
	define("ROOT_DIR", __DIR__);
}

require_once(dirname(__FILE__) . "/../config/config.php");

/**
* Function:  getConnection()
*
* This function gets an open PDO object.
*
* @return mixed Returns PDO if everything went OK, or false if something went wrong.
**/
function getConnection($retries = 3) {
	global $curSessionDb;

	if($curSessionDb != null) {
		return $curSessionDb;
	}

	try {
		
		$dbhost = DB_HOST;

		$dbuser = DB_USER;
		$dbpass = DB_PASS;
		$dbname = DB_NAME;
		$db = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=utf8", $dbuser, $dbpass);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
		$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

		// We want this to be set to true for this project because mysql native does not support one named parameter being bound in multiple different places in a query.
		// e.g. "INSERT INTO SomeTable (SomeField, DateEntered, DateModified) VALUES (:field, :CurTime, :CurTime)" is not allowed when ATTR_EMULATE_PREPARES is false.

	} catch(PDOException $e) {

        if($retries > 0) {
            // Attempt to get the connection again, as the error may have been an intermittent error.
            sleep(1);
            return getConnection($retries - 1);
        } else {
            return false;  
        }
	}

	$curSessionDb = $db;
	return $db;
}
