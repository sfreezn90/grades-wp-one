<?php 

require_once(dirname(__FILE__) . "/../config/config.php");

$db_host = DB_HOST; 
// Place the username for the MySQL database here
$db_username = DB_USER;  
// Place the password for the MySQL database here 
$db_pass = DB_PASS;  
// Place the name for the MySQL database here 
$db_name = DB_NAME; 

$conn = mysqli_connect($db_host, $db_username, $db_pass, $db_name);

$curSessionDb = null;

require_once(dirname(__FILE__) . "/../includes/inc_db.php");

?>