<?php
//Check to see the URL variable is set and that it exists in the database
if(isset($_GET['id'])){
    //Connect to MySQL database
include "sqlscripts/connect_to_mysql.php"; 
	$id = preg_replace('#[^0-9]#i', '', $_GET['id']);
    // Use this var to check to see if this ID exists, if yes then get the product 
	// details, if no then exit this script and give message why
    $sql = mysqli_query($conn, "SELECT * FROM subjects WHERE subject_id='$id' LIMIT 1");
    $subjectCount = mysqli_num_rows($sql); //count the output amount
    if ($subjectCount > 0){
        //get all the product details
        while($row = mysqli_fetch_array($sql)){
        $subject_id = $row["subject_id"];
        $subject = $row["subject"];
       }
        
        
    } else {
        $message = "That item does not exist.";
        echo "<script type='text/javascript'>alert('$message');</script>";
        exit();  
        
    }
} else {
    echo "Data to render this page is missing.";
    exit();
}

//Check to see the URL variable is set and that it exists in the database
if(isset($_GET['sid'])){
    //Connect to MySQL database
include "sqlscripts/connect_to_mysql.php"; 
	$s_id = preg_replace('#[^0-9]#i', '', $_GET['sid']);
    // Use this var to check to see if this ID exists, if yes then get the product 
	// details, if no then exit this script and give message why
    $sql = mysqli_query($conn, "SELECT * FROM students WHERE student_id='$s_id' LIMIT 1");
    $studentCount = mysqli_num_rows($sql); //count the output amount
    if ($studentCount > 0){
        //get all the product details
        while($row = mysqli_fetch_array($sql)){
        $s_id = $row["student_id"];
        $first_name = $row["first_name"];
        $last_name = $row["last_name"];
       }
        
        
    } else {
        $message = "That person does not exist.";
        echo "<script type='text/javascript'>alert('$message');</script>";
        exit();  
        
    }
} else {
    echo "Data to render this page is missing.";
    exit();
}


//Pulls up student list for viewing
$student="";
$sql = mysqli_query($conn, "SELECT * FROM students WHERE student_id = $s_id");
$subjectCount = mysqli_num_rows($sql); //count the output amount
if ($subjectCount > 0){
    while($row = mysqli_fetch_array($sql)){
        $student_id = $row["student_id"];
        $first_name = $row["first_name"];
        $last_name = $row["last_name"];
        $grade = $row["grade"];
        $student .=  ''. $first_name .' '. $last_name .'';
    }
} else {
    $student = "This student is not in the system.";
}


//Pulls up student list for viewing
$gradeList="";
$sql = mysqli_query($conn, "SELECT AVG (daily_grade) FROM grades WHERE student_id = $s_id AND subject_id = $id");
$personCount = mysqli_num_rows($sql); //count the output amount
if ($personCount > 0){
    while($row = mysqli_fetch_array($sql)){
        $daily_avg = $row["AVG (daily_grade)"];
        $gradeList .=  '<p>' . $subject . ' '. $daily_avg .'</p>';
    }
} else {
    $gradeList = "You have not entered any grades yet.";
}


mysqli_close($conn);

?>

    <html>

    <head>
        <title><?php echo $subject ?></title>
        <?php include_once("header.php");?>
    </head>

    <body>
        <div id="body">
            <h2><?php echo $student ?> Averages</h2>
            <?php echo $gradeList; ?>
        </div>
    </body>

    </html>