<?php

//Check to see the URL variable is set and that it exists in the database
if(isset($_GET['sid'])){
    //Connect to MySQL database
include "sqlscripts/connect_to_mysql.php"; 
	$s_id = preg_replace('#[^0-9]#i', '', $_GET['sid']);
    // Use this var to check to see if this ID exists, if yes then get the product 
	// details, if no then exit this script and give message why
    $sql = mysqli_query($conn, "SELECT * FROM students WHERE student_id='$s_id' LIMIT 1");
    $studentCount = mysqli_num_rows($sql); //count the output amount
    if ($studentCount > 0){
        //get all the product details
        while($row = mysqli_fetch_array($sql)){
        $s_id = $row["student_id"];
        $first_name = $row["first_name"];
        $last_name = $row["last_name"];
       }
        
        
    } else {
        $message = "That person does not exist.";
        echo "<script type='text/javascript'>alert('$message');</script>";
        exit();  
        
    }
} else {
    echo "Data to render this page is missing.";
    exit();
}


//Pulls up student list for viewing
$student="";
$sql = mysqli_query($conn, "SELECT * FROM students WHERE student_id = $s_id");
$subjectCount = mysqli_num_rows($sql); //count the output amount
if ($subjectCount > 0){
    while($row = mysqli_fetch_array($sql)){
        $student_id = $row["student_id"];
        $first_name = $row["first_name"];
        $last_name = $row["last_name"];
        $grade = $row["grade"];
        $student .=  ''. $first_name .' '. $last_name .'';
    }
} else {
    $student = "This student is not in the system.";
}


//Pulls up student list for viewing
$gradeList="";
$sql = mysqli_query($conn, 
                    "SELECT s.subject, g.subject_id, s.subject_id, g.student_id, 
                        (SELECT AVG(daily_grade)
                            FROM grades WHERE student_id = $s_id) AS average 
                            FROM grades g 
                            INNER JOIN subjects s 
                            ON g.subject_id = s.subject_id 
                            WHERE g.student_id = $s_id
                            AND g.daily_grade IS NOT NULL");
$personCount = mysqli_num_rows($sql); //count the output amount
if ($personCount > 0){
    while($row = mysqli_fetch_array($sql)){
        $daily_avg = $row["average"];
        $subject = $row["subject"];
        $gradeList .=  '<p>' . $subject . ' '. $daily_avg .'</p>';
    }
} else {
    $gradeList = "You have not entered any grades yet.";
}

$quizGrade="";
$sql = mysqli_query($conn, 
                    "SELECT s.subject, g.student_id, 
                        (SELECT AVG(quiz_grade)
                            FROM grades WHERE student_id = $s_id) AS average 
                            FROM grades g 
                            INNER JOIN subjects s 
                            ON g.subject_id = s.subject_id 
                            WHERE g.student_id = $s_id
                            AND g.quiz_grade IS NOT NULL");
$personCount = mysqli_num_rows($sql); //count the output amount
if ($personCount > 0){
    while($row = mysqli_fetch_array($sql)){
        $quiz_avg = $row["average"];
        $subject = $row["subject"];
        $quizGrade .=  '<p>' . $subject . ' '. $quiz_avg .'</p>';
    }
} else {
    $quizGrade = "You have not entered any quiz grades yet.";
}

mysqli_close($conn);

?>

    <html>

    <head>
        <title><?php echo $subject ?></title>
        <?php include_once("header.php");?>
    </head>

    <body>
        <div id="body">
            <h2><?php echo $student ?></h2>
            <h4>Daily Grade Averages</h4>
            <?php echo $gradeList; ?>
            <div>
                <h4>Quiz Grade Averages</h4>
                <?php echo $quizGrade; ?>
            </div>
        </div>
    </body>

    </html>