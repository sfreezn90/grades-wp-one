<?php
//Check to see the URL variable is set and that it exists in the database
if(isset($_GET['id'])){
    //Connect to MySQL database
include "sqlscripts/connect_to_mysql.php"; 
	$id = preg_replace('#[^0-9]#i', '', $_GET['id']);
    // Use this var to check to see if this ID exists, if yes then get the product 
	// details, if no then exit this script and give message why
    $sql = mysqli_query($conn, "SELECT * FROM subjects WHERE subject_id='$id' LIMIT 1");
    $subjectCount = mysqli_num_rows($sql); //count the output amount
    if ($subjectCount > 0){
        //get all the product details
        while($row = mysqli_fetch_array($sql)){
        $subject_id = $row["subject_id"];
        $subject = $row["subject"];
       }
        
        
    } else {
        $message = "That item does not exist.";
        echo "<script type='text/javascript'>alert('$message');</script>";
        exit();  
        
    }
} else {
    echo "Data to render this page is missing.";
    exit();
}


//Pulls up student list for viewing
$studentList8="";
$sql = mysqli_query($conn, "SELECT * FROM students WHERE grade = 8");
$subjectCount = mysqli_num_rows($sql); //count the output amount
if ($subjectCount > 0){
    while($row = mysqli_fetch_array($sql)){
        $student_id = $row["student_id"];
        $first_name = $row["first_name"];
        $last_name = $row["last_name"];
        $grade = $row["grade"];
        $studentList8 .=  '
        
            <div class="form-group">
                <input name="student_id[]" type="hidden" value="'. $student_id . '"/>
                <label for="grade">' . $first_name . ' ' . $last_name . '</label>
                <input type="text" class="form-control" name="grade[]" id="grade" size="3">
            </div><br/>';
    }
} else {
    $studentList8 = "You have not entered any students yet.";
}

//Pulls up student list for viewing
$studentList7="";
$sql = mysqli_query($conn, "SELECT * FROM students WHERE grade = 7");
$subjectCount = mysqli_num_rows($sql); //count the output amount
if ($subjectCount > 0){
    while($row = mysqli_fetch_array($sql)){
        $student_id = $row["student_id"];
        $first_name = $row["first_name"];
        $last_name = $row["last_name"];
        $grade = $row["grade"];
        $studentList7 .=  '
        
            <div class="form-group">
                <input name="student_id[]" type="hidden" value="'. $student_id . '"/>
                <label for="grade">' . $first_name . ' ' . $last_name . '</label>
                <input type="text" class="form-control" name="grade[]" id="grade" size="3">
            </div><br/>';
    }
} else {
    $studentList7 = "You have not entered any students yet.";
}

mysqli_close($conn);

?>

    <html>

    <head>
        <title><?php echo $subject ?></title>
        <?php include_once("header.php");?>
    </head>

    <body>
        <div id="body">
            <h2><?php echo $subject ?> Quiz Grades</h2>
            <form class="form-inline"  action="ajax/quiz_content.php" enctype="multipart/form-data" name="myForm" id="myform" method="post">
                <h4>Grade 7</h4>
                <?php echo $studentList7; ?>
                <h4>Grade 8</h4>
                <?php echo $studentList8; ?>
                <input name="subject" value="<?php echo $subject_id ?>" type="hidden"/>
                <input type="submit" id="myForm" class="btn btn-default" value="Submit Grades">
            </form>
        </div>
    </body>

    </html>