<?php
include "sqlscripts/connect_to_mysql.php"; 

//Pulls up subject list for viewing.
$subjectList="";
$sql = mysqli_query($conn, "SELECT * FROM subjects");
$subjectCount = mysqli_num_rows($sql); //count the output amount
if ($subjectCount > 0){
    while($row = mysqli_fetch_array($sql)){
        $subject_id = $row["subject_id"];
        $subject = $row["subject"];
        $subjectList .=  '
        <div>
            <a href="subject.php?id=' . $subject_id . '"><p>' . $subject . '</p></a>
        </div>'
        ;
    }
} else {
    $subjectList = "You have not entered any subjects yet.";
}

//Pulls up subject list for viewing.
$quizList="";
$sql = mysqli_query($conn, "SELECT * FROM subjects");
$subjectCount = mysqli_num_rows($sql); //count the output amount
if ($subjectCount > 0){
    while($row = mysqli_fetch_array($sql)){
        $subject_id = $row["subject_id"];
        $subject = $row["subject"];
        $quizList .=  '
        <div>
            <a href="quiz.php?id=' . $subject_id . '"><p>' . $subject . '</p></a>
        </div>'
        ;
    }
} else {
    $quizList = "You have not entered any subjects yet.";
}

//Pulls up subject list for viewing.
$testList="";
$sql = mysqli_query($conn, "SELECT * FROM subjects");
$subjectCount = mysqli_num_rows($sql); //count the output amount
if ($subjectCount > 0){
    while($row = mysqli_fetch_array($sql)){
        $subject_id = $row["subject_id"];
        $subject = $row["subject"];
        $testList .=  '
        <div>
            <a href="test.php?id=' . $subject_id . '"><p>' . $subject . '</p></a>
        </div>'
        ;
    }
} else {
    $quizList = "You have not entered any subjects yet.";
}

mysqli_close($conn);

?>

    <html>

    <head>
        <title>Subject List</title>
        <?php include_once("header.php");?>
    </head>

    <body>
        <div id="body">
            <h2>Enter Daily Grades</h2>
            <?php echo $subjectList; ?>
            <h2>Enter Quiz Grades</h2>
            <?php echo $quizList; ?>
            <h2>Enter Test Grades</h2>
            <?php echo $testList; ?>
        </div>
    </body>

    </html>